# IMAGEN BASE
FROM bash:latest

# CONFIGURATION
# Actualizar la lista de paquetes
RUN apt-get update
# Instalar ssh y sshpass
RUN apt-get install -y ssh sshpass

CMD ["echo", "Hello world"]